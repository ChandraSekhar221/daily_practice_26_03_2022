const mydata = require('./data') 

// 2.Convert all the salary values into proper numbers instead of strings 

const convertedSalariesarray = mydata.map((employee) => { 
    employee.salary = parseFloat(employee.salary.replace("$",""))
    return employee
})
    
console.log(convertedSalariesarray)