const mydata = require('./data') 

// 6. Find the average salary of based on country using only HOF method

const averageSalariesofCountries = mydata.reduce((startingvalue,employee) => {
    if(!startingvalue[employee.location]) {
        startingvalue[employee.location] = {'count': 0 , 'totalSalary': 0 , 'averageSalary': 0 }
    }
    startingvalue[employee.location].count += 1 ;
    startingvalue[employee.location].totalSalary += parseFloat(employee.salary.replace('$',''))*10000 
    startingvalue[employee.location].averageSalary = (startingvalue[employee.location].totalSalary /startingvalue[employee.location].count ).toFixed(2)
    return startingvalue
}, {})

const countries = Object.entries(averageSalariesofCountries);

const averageSalariesPerCountry = countries.reduce((initialValue,countrySalaryDetails) => {
    initialValue[countrySalaryDetails[0]] = countrySalaryDetails[1].averageSalary
    return initialValue 
}, {})
console.log(averageSalariesPerCountry)