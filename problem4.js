const mydata = require('./data') 

// 4. Find the sum of all salaries 

const reducer = (initialValue,currentValue) => {
    let salary = parseFloat(currentValue.salary.replace("$","")) * 10000 ;
    return initialValue + salary ;
}

const result = mydata.reduce(reducer,0)
console.log(result)