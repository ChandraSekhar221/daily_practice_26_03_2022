const mydata = require('./data') 

// 3. Assume that each salary amount is a factor of 10000 and correct it but add it ---
// --> as a new key (corrected_salary or something)

const newSalaryAddedArray = mydata.map((employee) => {
    employee['modifiedSalary'] = parseFloat(employee.salary.replace("$",""))*10000
    return employee
})
console.log(newSalaryAddedArray)

