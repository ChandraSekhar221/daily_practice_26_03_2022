const mydata = require('./data') 

// 5. Find the sum of all salaries based on country using only HOF method

const sumOfCountrySalaries = mydata.reduce((startingValue, employee) => {
    if(!startingValue[employee.location]) {
        startingValue[employee.location] = 0 ;
    }
    startingValue[employee.location] += parseFloat(employee.salary.replace('$',"")) * 10000 ;
    return startingValue ;
}, {})

console.log(sumOfCountrySalaries)